<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright (C) 2018-2020 Stephan Kreutzer

This file is part of pattern-template-form-generator.

pattern-template-form-generator is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

pattern-template-form-generator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with pattern-template-form-generator. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns="http://www.w3.org/1999/xhtml" xmlns:src="htx-scheme-id://org.groupware-systems.20190227T133048Z/meta-pattern/pattern-template.20200511T001800Z">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="no" doctype-public="-//W3C//DTD XHTML 1.1//EN" doctype-system="http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd"/>

  <xsl:template match="/">
    <html version="-//W3C//DTD XHTML 1.1//EN" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.w3.org/1999/xhtml http://www.w3.org/MarkUp/SCHEMA/xhtml11.xsd" xml:lang="en" lang="en">
      <head>
        <meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8"/>
<xsl:text>&#xA;</xsl:text>
<xsl:comment> This file was created by form_generator_1.xsl of pattern-template-form-generator, which is free software licensed under the GNU Affero General Public License 3 or any later version (see https://gitlab.com/skreutzer/pattern-template-form-generator/). </xsl:comment>
<xsl:text>&#xA;</xsl:text>
<xsl:comment>
Copyright (C) 2018-2020 Stephan Kreutzer

This file is part of pattern-template-form-generator.

pattern-template-form-generator is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License version 3 or any later version,
as published by the Free Software Foundation.

pattern-template-form-generator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License 3 for more details.

You should have received a copy of the GNU Affero General Public License 3
along with pattern-template-form-generator. If not, see &lt;http://www.gnu.org/licenses/&gt;.
</xsl:comment>
<xsl:text>&#xA;</xsl:text>
        <title>
          <xsl:value-of select="./src:pattern-template/@title"/>
        </title>
        <meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport"/>
        <script type="text/javascript">
<xsl:text>
          "use strict";

          function save()
          {
              let xml = "&lt;?xml version=\"1.0\" encoding=\"UTF-8\"?&gt;&lt;</xsl:text>
              <xsl:value-of select="./src:pattern-template/@name"/>
              <xsl:text> xmlns=\"</xsl:text>
              <xsl:value-of select="./src:pattern-template/@xmlns-id"/>
              <xsl:text>\"&gt;</xsl:text>

              <xsl:for-each select="././src:pattern-template/src:section">
                <xsl:text>&lt;</xsl:text>
                <xsl:value-of select="@name"/>
                <xsl:text>&gt;" + </xsl:text>

                <xsl:text>document.getElementById('</xsl:text>
                <xsl:value-of select="@name"/>
                <!-- Ampersand needs to come first, otherwise it'll mess up previous results. -->
                <xsl:text>').value.replace(/&amp;/g, '&amp;amp;').replace(/&gt;/g, '&lt;').replace(/&lt;/g, '&amp;gt;').replace(/"/g, '&amp;quot;').replace(/'/g, '&amp;apos;') /* Browsers and JavaScript, dumbest shit on earth! */</xsl:text>

                <xsl:text> + "&lt;/</xsl:text>
                <xsl:value-of select="@name"/>
                <xsl:text>&gt;</xsl:text>
              </xsl:for-each>

              <xsl:text>&lt;/</xsl:text>
              <xsl:value-of select="./src:pattern-template/@name"/>
              <xsl:text>&gt;";
              const blob = new Blob([xml], { "type": "application/xml" });
              const blobUrl = window.URL.createObjectURL(blob);
              //window.location.href = blobUrl;
              window.open(blobUrl, '');

              /*
              // Might hit some length limit.
              const blob = "data:application/xml;charset=utf-8," + encodeURIComponent(xml);
              window.open(blob, '');
              //window.location.href = blob;
              */
          }

          window.onload = function() {
              document.getElementById("template_form").onsubmit = function (event) {
                  event.preventDefault();

                  save();
              }
          };
</xsl:text>
        </script>
      </head>
      <body>
        <div>
          <h1>
            <xsl:value-of select="./src:pattern-template/@title"/>
          </h1>
          <form id="template_form" action="#" method="post">
            <fieldset class="table">
              <xsl:apply-templates select="./src:pattern-template/src:section"/>
              <input type="submit" value="save"/>
            </fieldset>
          </form>
        </div>
      </body>
    </html>
  </xsl:template>

  <xsl:template match="/src:pattern-template/src:section">
    <div>
      <div>
        <label for="{@name}">
          <xsl:value-of select="@title"/>
        </label>
      </div>
      <div>
        <textarea type="text" name="{@name}" id="{@name}"/>
      </div>
    </div>
  </xsl:template>

  <xsl:template match="node()|text()|@*"/>

</xsl:stylesheet>
